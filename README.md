# Métodos Computacionales I - Trabajo Práctico 3

En este repositorio se encuentran los comandos utilizados para la resolución de las tareas en el trabajo práctico, modificando el nombre de los archivos, generando archivos de salida y moviendo dichos archivos entre carpetas. Además, se menciona la utilidad del comando "tar" y una de sus utilizaciones en este trabajo.